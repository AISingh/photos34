package model;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import javax.imageio.ImageIO;

/**
 * Class representing an album.
 * @author Dylan Oelkers
 * @author Aman Singh
 *
 */
public class Album implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * Name of album.
	 */
	private String name;
	/**
	 * Photos in the album.
	 */
	private ArrayList<Photo> photos;
	/**
	 * Thumbnail of the album.
	 */
	private Photo thumbnail;
	
	public Album(String name) {
		this.name = name;
		photos = new ArrayList<Photo>();
	}

	/**
	 * Gets photos in the album.
	 * @return ArrayList of photos.
	 */
	public ArrayList<Photo> getPhotos(){
		return photos;
	}
	
	/**
	 * Adds stock photos to the album, which are located in
	 * the stock_photos directory.
	 */
	public void addStockPhotos() {
		int index=1;
		for (int i=0; i<4; i++) {
			String fileLoc = "stock_photos/stock_"+ index + ".jpg";
			BufferedImage img = null;
			try {
				img = ImageIO.read(new File(fileLoc));
			} catch (IOException e){
				e.printStackTrace();
			}
			Photo photo = new Photo(img, "Stock " +index, fileLoc, "10/17/17");
			photo.addTag(new Tag("stock", "stock"));
			photos.add(photo);
			photo.setAlbum(this);
			index++;
		}
	}
	
	/**
	 * Adds photo to album.
	 * @param photo Photo to be added.
	 */
	public void addImage(Photo photo) {
		photos.add(photo);
		photo.setAlbum(this);
		
		if (this.photos.size() == 1) {
			this.thumbnail = photo;
		}
	}
	
	public Photo getThumbnail() {
		return this.thumbnail;
	}
	
	/**
	 * Removes photo from album.
	 * @param photo Photo to be removed.
	 */
	public void removeImage(Photo photo) {
		photos.remove(photo);
	}
	
	/**
	 * Renames the album.
	 * @param newName New name of album.
	 */
	public void rename(String newName) {
		name = newName;
	}
	
	/**
	 * Gets name of album.
	 * @return Name of album.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Gets number of photos in album.
	 * @return Number of photos.
	 */
	public int getPhotoCount() {
		return this.photos.size();
	}
	
	/**
	 * Gets latest photo date in the album.
	 * @return String of latest date.
	 */
	public String getLatestPhotoDate() {
		Photo latest = null;
		
		if(this.photos.size()!=0)
		latest = this.photos.get(0);
		
		if (this.photos.size() < 1) {
			return "none";
		}
		
		for (Photo ph : this.photos) {
			latest = (ph.getDateDate().after(latest.getDateDate())) ? ph : latest;
		}
		return latest.getDate();	
	}
	
	/**
	 * Gets earliest photo date in the album.
	 * @return String of earliest photo date.
	 */
	public String getEarliestPhotoDate() {
		Photo earliest = null;
		
		if(this.photos.size()!=0)
		earliest = this.photos.get(0);
		
		if(this.photos.size() < 1) {
			return "none";
		}
		
		for (Photo ph : this.photos) {
			earliest = (ph.getDateDate().before(earliest.getDateDate())) ? ph : earliest;
		}
		return earliest.getDate();	
	}

}
