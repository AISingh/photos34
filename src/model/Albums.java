package model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Class representing a list of albums.
 * @author Dylan Oelkers
 * @author Aman Singh
 *
 */
public class Albums implements Serializable{
	
	/**
	 * List of albums.
	 */
	private ArrayList<Album> albums;
	
	public Albums(){
		this.albums = new ArrayList<Album>();
	}
	
	/**
	 * Adds album to list.
	 * @param newAlbum Album to be added.
	 */
	public void addAlbum(Album newAlbum){
		this.albums.add(newAlbum);
	}

	/**
	 * Removes album from list.
	 * @param album Album to be removed.
	 */
	public void deleteAlbum(Album album){
		this.albums.remove(album);
	}
	
	/**
	 * Gets list of albums.
	 * @return List of albums.
	 */
	public ArrayList<Album> getAlbums(){
		return this.albums;
	}
	
	/**
	 * Gets photos that are within a certain date range, returns them as an album.
	 * @param startDate Starting date.
	 * @param endDate Ending date.
	 * @return Album of resulting photos.
	 */
	public Album getPhotosByDate(Date startDate, Date endDate){
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		Album album = new Album("Results from " + format.format(startDate) + " to " + format.format(endDate));
		
		for(Album al : albums) {
			for(Photo ph : al.getPhotos()){
				if (!(ph.getDateDate().before(startDate) || ph.getDateDate().after(endDate))) {
					album.addImage(Photo.copy(ph));
				}
			}
		}
		
		return album;
	}
	
	/**
	 * Gets list of photos that have a certain tag across all albums, puts them in new album.
	 * @param tags Tags to be searched for.
	 * @return Album of photos with those tags.
	 */
	public Album getPhotosByTags(ArrayList<Tag> tags){
		Album album = new Album("Results from tags");
		
		for(Album al : albums) {
			for(Photo ph : al.getPhotos()){
				boolean add = true;
				for(Tag t: tags) {
					add = ph.searchTag(t);
					if(!add) break;
				}
				if(add){
					album.addImage(Photo.copy(ph));
				}
			}
		}
		
		return album;
	}
}