package model;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Class representing a user.
 * 
 * @author Dylan Oelkers
 * @author Aman Singh
 *
 */
public class User implements Serializable {
	/**
	 * Username
	 */
	private String username;
	/**
	 * Password.
	 */
	private String password;
	/**
	 * List of albums associated with that user.
	 */
	private Albums albums;
	
	private Album currAlbum; //album opened

	
	// private boolean isAdmin = false;
	/**
	 * ArrayList of all users.
	 */
	private static ArrayList<User> users = new ArrayList<User>();
	
	public User(String username, String password) {
		this.username = username;
		this.password = password;
		albums = new Albums();
		Album stock = new Album("Stock");
		stock.addStockPhotos();
		//stock.setAid(0);
		albums.addAlbum(stock);
	}
	
	/**
	 * Gets username.
	 * @return Username
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * Gets password.
	 * @return Password
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * Gets list of all users.
	 * @return ArrayList of users.
	 */
	public static ArrayList<User> getUsers(){
		return users;
	}
	
	/**
	 * Removes user from list of users.
	 * @param user User to be removed.
	 */
	public static void removeUser(User user) {
		users.remove(user);
	}
	
	/**
	 * Sets list of users by reading in from the users.dat file using
	 * serialization.
	 */
	public static void setUsers() {
		
		try {
	         FileInputStream fileIn = new FileInputStream("src/model/users.dat");
	         if (fileIn.available()==0) {
	        	 fileIn.close();
	        	 return;
	         }
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         int userCount = in.readInt();
	         for(int i=0; i<userCount; i++) {
	        	 users.add((User) in.readObject());
	         }
	         in.close();
	         fileIn.close();
	      } catch (IOException i) {
	         i.printStackTrace();
	         return;
	      } catch (ClassNotFoundException c) {
	         System.out.println("User class not found");
	         c.printStackTrace();
	         return;
	      }
	}
	
	/**
	 * Returns user as string of username and password.
	 */
	public String toString() {
		return "User: " + username + " Password: " + password;
	}
	
	/**
	 * Adds album to user's albums.
	 * @param album Album to be added.
	 */
	public void addAlbum(Album album) {
		albums.addAlbum(album);
	}
	
	/**
	 * Adds album to user's albums.
	 * @param name String name of album to be added.
	 */
	public void addAlbum(String name) {
		Album album = new Album(name);
		albums.addAlbum(album);
	}
	
	/**
	 * Removes album from user's albums.
	 * @param album Album to be removed.
	 */
	public void removeAlbum(Album album) {
		albums.deleteAlbum(album);
	}
	
	/**
	 * Gets user's albums.
	 * @return List of albums.
	 */
	public Albums getAlbums() {
		return this.albums;
	}
	
	public void setCurrAlbum(Album album) {
		currAlbum = album;
	}

	public Album getCurrAlbum() {
		return currAlbum;
	}
	
}