package model;

import java.io.Serializable;

/**
 * Class representing a tag of a photo.
 * 
 * @author Dylan Oelkers
 * @author Aman Singh
 *
 */

public class Tag implements Serializable {
	/**
	 * Value of the tag (i.e., Mark, New York, etc.)
	 */
	private String value;
	/**
	 * Name of the tag (i.e. Person, Location)
	 */
	private String name;
	
	public Tag(String value, String name) {
		this.value = value;
		this.name = name;
	}
	/**
	 * Gets value of tag.
	 * @return Value of tag.
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Gets name of tag.
	 * @return Name of tag.
	 */
	public String getName() {
		return name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Tag)) {
			return false;
		}
		Tag t = (Tag) obj;
		return (this.name.equals(t.name) && this.value.equals(t.value));
	}
}
