package model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.imageio.ImageIO;

import java.awt.image.BufferedImage;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.*;

/**
 * Class representing a photo.
 * 
 * @author Dylan Oelkers
 * @author Aman Singh
 *
 */

public class Photo implements Serializable {
	
	/*
	 *  Must translate image to normal java images when writing objects!
	 */
	/**
	 * Image the photo is displaying.
	 */
	private transient BufferedImage image;
	/**
	 * List of tags on the photo.
	 */
	private ArrayList<Tag> tags;
	/**
	 * Capiton of the photo.
	 */
	private String caption;
	/**
	 * Where the file is located on the user's PC.
	 */
	private String fileLocation;
	/**
	 * String version of the date of the photo.
	 */
	private String date; // Edited each time a change to photo is made
	/**
	 * Date version of the date of the photo.
	 */
	private Date date2;
	/**
	 * Album the photo belongs to.
	 */
	private Album album;
	
	public Photo(BufferedImage image, String caption, String fileLocation, String date) {
		this.image = image;
		this.caption = caption;
		this.fileLocation = fileLocation;
		this.date = date;
		//this.pid = pid;
		tags = new ArrayList<Tag>();
	}
	
	/**
	 * Sets the album of the photo.
	 * @param album Album to which the photo will belong.
	 */
	public void setAlbum(Album album) {
		this.album = album;
	}
	
	/**
	 * Returns album of which the photo belongs.
	 * @return Album the photo belongs to.
	 */
	public Album getAlbum() {
		return album;
	}
	
	/**
	 * Returns image of the photo.
	 * @return Image of the photo.
	 */
	public BufferedImage getImage() {
		return image;
	}
	
	/**
	 * Gets file location.
	 * @return File location.
	 */
	public String getFileLocation() {
		return fileLocation;
	}
	
	/**
	 * Gets string version of the date.
	 * @return
	 */
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date=date;
	}
	
	/**
	 * Gets Date version of date.
	 * @return
	 */
	public Date getDateDate() {
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		try {
			date2=format.parse(this.date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date2;
	}
	
	public LocalDate getDateLocalDate() {
		return LocalDate.parse(this.date, DateTimeFormatter.ofPattern("MM/dd/yyyy"));
	}
	
	/**
	 * Gets tags of the photo.
	 * @return ArrayList of tags.
	 */
	public ArrayList<Tag> getTags(){
		return tags;
	}
	
	/**
	 * Adds tag and edits photo date.
	 * @param tag Tag to be added.
	 */
	public void addTag(Tag tag) {
		tags.add(tag);
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MILLISECOND,0);
		Date calDate = cal.getTime();
		String pattern = "MM/dd/yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		date = simpleDateFormat.format(calDate);
	}
	
	/**
	 * Removes tag and edits date of photo.
	 * @param tag Tag to be removed.
	 */
	public void removeTag(Tag tag) {
		tags.remove(tag);
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MILLISECOND,0);
		Date calDate = cal.getTime();
		String pattern = "MM/dd/yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		date = simpleDateFormat.format(calDate);
	}
	
	/**
	 * Searches for specific tag on the photo.
	 * @param tag Tag to be searched for.
	 * @return True if tag found, false otherwise.
	 */
	public boolean searchTag(Tag tag) {
		for(Tag t: tags) {
			if(t.equals(tag)) return true;
		}
		return false;
	}
	
	/**
	 * Gets caption of the photo.
	 * @return String of the caption.
	 */
	public String getCaption() {
		return caption;
	}
	
	public void setCaption(String caption) {
		this.caption=caption;
	}
	
	/**
	 * Changes the caption of the photo and edits date.
	 * @param newCaption New caption.
	 */
	public void changeCaption(String newCaption) {
		caption = newCaption;
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MILLISECOND,0);
		Date calDate = cal.getTime();
		String pattern = "MM/dd/yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		date = simpleDateFormat.format(calDate);
	}
	
	/**
	 * Writes object to outputstream, implementing Serializable.
	 * @param out ObjectOutputStream
	 * @throws IOException
	 */
	private void writeObject(ObjectOutputStream out) throws IOException {
	        out.defaultWriteObject();
	        ImageIO.write(image, "png", out);
	}
	
	/**
	 * Reads object from inputstream, implementing Serializable.
	 * @param in ObjectInputStream
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
	        in.defaultReadObject();
	        image = ImageIO.read(in);
	}
	
	/**
	 * Converts photo's image to a JavaFX version.
	 * @return WriteableImage representing the photo's image.
	 */
	public WritableImage convertToFXImage() {
		return SwingFXUtils.toFXImage(image, null);
	}
	
	/**
	 * Copies photo.
	 * @param ph Photo to be copied.
	 * @return The new copy.
	 */
	public static Photo copy(Photo ph) {
		Photo newPhoto = new Photo(ph.image, ph.caption, ph.fileLocation, ph.date);
		for (Tag tag : ph.getTags()) {
			newPhoto.addTag(tag);
		}
		return newPhoto;
	}
}
