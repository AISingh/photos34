package application;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import model.User;
import view.AdminController;
import view.LoginController;

/**
 * Application for storing and viewing photos. It stores multiple users,
 * and each user can create albums, add photos, add tags to photos, and search
 * for photos based on tag values.
 * 
 * @author Dylan Oelkers
 * @author Aman Singh
 *
 */

public class Photos extends Application {
	
	@Override
	public void start(Stage primaryStage) throws IOException {
		User.setUsers();
		FXMLLoader loginLoader = new FXMLLoader();
		loginLoader.setLocation(getClass().getResource("/view/Login.fxml"));
   	    Pane root = (Pane)loginLoader.load();
   	    
        primaryStage.setTitle("Photos");
        primaryStage.setScene(new Scene(root, 700, 500));
        
        LoginController login_Controller = loginLoader.getController();
        login_Controller.start(primaryStage);
        primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
