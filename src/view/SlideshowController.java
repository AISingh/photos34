package view;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Album;
import model.User;

/**
 * Controller for slideshow page.
 * 
 * @author Dylan Oelkers
 * @author Aman Singh
 *
 */
public class SlideshowController {
	@FXML
	Button back = new Button();
	@FXML
	Button logout = new Button();
	@FXML
	Text ssCaption;
	@FXML
	ImageView ssPic;
	@FXML
	Text ssAlbum;
	@FXML
	Button ssNext;
	@FXML
	Button ssPrev;
	
	/**
	 * Index of photo to be shown.
	 */
	private int index = 0;
	public void start(Stage mainStage) {
		Album album = AlbumController.currAlbum;
		
		ssAlbum.setText(album.getName());
		ssCaption.setText(album.getPhotos().get(index).getCaption());
		ssPic.setFitHeight(452);
		ssPic.setFitWidth(816);
		
		ssPic.setImage(SwingFXUtils.toFXImage(album.getPhotos().get(index).getImage(), null));
		
		ssNext.setOnAction(e -> {
			if(index+1==album.getPhotoCount()) {
				return;
			}
			index++;
			ssPic.setImage(SwingFXUtils.toFXImage(album.getPhotos().get(index).getImage(), null));
			ssCaption.setText(album.getPhotos().get(index).getCaption());
		});
		
		ssPrev.setOnAction(e -> {
			if(index-1<0) {
				return;
			}
			index--;
			ssPic.setImage(SwingFXUtils.toFXImage(album.getPhotos().get(index).getImage(), null));
			ssCaption.setText(album.getPhotos().get(index).getCaption());
		});
		
		back.setOnAction(e -> {
			FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(getClass().getResource("/view/Album.fxml"));
	   	    VBox root = null;
			try {
				root = (VBox)loader.load();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	        mainStage.setScene(new Scene(root, 900, 600));
	       
	        AlbumController album_Controller = (AlbumController) loader.getController();
	        album_Controller.start(mainStage);
	        
	        mainStage.show();
		});
		
		logout.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				try {
					 // Empties file before writing to it
					 File file = new File("src/model/users.dat");
					 PrintWriter pw = new PrintWriter(file);
					 pw.write("");
					 pw.close();
					 
			         FileOutputStream fileOut =
			         new FileOutputStream("src/model/users.dat");
			         ObjectOutputStream out = new ObjectOutputStream(fileOut);
			         out.writeInt(User.getUsers().size()); 
			         for (User user : User.getUsers()) {
			        	 out.writeObject(user);
			         }
			         out.close();
			         fileOut.close();
			         System.out.println("Serialized data is saved in users.dat");
			      } catch (IOException i) {
			         i.printStackTrace();
			      }
				// Sends back to login page
				FXMLLoader loader = new FXMLLoader();
		        loader.setLocation(getClass().getResource("/view/Login.fxml"));
		   	    Pane root = null;
				try {
					root = (Pane)loader.load();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
		        mainStage.setScene(new Scene(root, 675, 425));
		        LoginController login_Controller = (LoginController) loader.getController();
		        login_Controller.start(mainStage);
		        LoginController.uloggedIn = null;
		        mainStage.show();
			}
		});
	}
}
