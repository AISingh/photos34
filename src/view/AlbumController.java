package view;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.imageio.ImageIO;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.Tag;
import model.User;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Controller for album page, when a user opens an album. Users can
 * add, edit, or remove photos, play a slideshow, and copy or move 
 * photos to a different album.
 * 
 * @author Dylan Oelkers
 * @author Aman Singh
 *
 */
public class AlbumController {
	
	@FXML
	Button slideshow = new Button();
	@FXML
	Button backToAlbums = new Button();
	@FXML
	Button logout = new Button();
	@FXML
	ListView<Photo> photosList = new ListView<>();
	@FXML
	ImageView selectedAlbumPhoto;
	@FXML
	Text selectedPhotoCaption;
	@FXML
	Button addPhoto = new Button();
	@FXML
	Button removePhoto = new Button();
	@FXML
	Button editPhoto = new Button();
	@FXML
	Text date;
	@FXML
	MenuButton copyTo;
	@FXML
	MenuButton moveTo;
	@FXML
	ListView<String> tags = new ListView<>();
	
	/**
	 * Observable list representing the photos in the album.
	 */
	private ObservableList<Photo> photosObs = FXCollections.observableArrayList();
	/**
	 * Observable list representing the tags of the photo.
	 */
	private ObservableList<String> tagsObs = FXCollections.observableArrayList();
	/**
	 * The current album that the page is displaying
	 */
	static Album currAlbum;
	
	public static Photo currPhoto;
	
	public void start(Stage mainStage) {
		
		/*
		 * below code for stock user is for testing purposes
		 */
		
		
		User currUser = LoginController.uloggedIn;
		
		currAlbum = currUser.getCurrAlbum();
		
		if(currAlbum.getPhotoCount()!=0) {
		for (Photo photo : currAlbum.getPhotos()) {
			photosObs.add(photo);
		}
		
		photosList.setItems(photosObs);
		
		photosList.getSelectionModel().select(0);
		
		Photo photo = photosList.getSelectionModel().getSelectedItem();
		currPhoto=photo;
		String captionObs = photosList.getSelectionModel().getSelectedItem().getCaption();
    	selectedPhotoCaption.setText(captionObs);
  
    	date.setText(photo.getDate());
    	
    	for (Tag tag : photo.getTags()) {
    		tagsObs.add(tag.getName() + ": " + tag.getValue());
    	}
    	
    	tags.setItems(tagsObs);
    	selectedAlbumPhoto.setFitHeight(225);
    	selectedAlbumPhoto.setFitWidth(250);
    	selectedAlbumPhoto.setImage(SwingFXUtils.toFXImage(photo.getImage(), null));
		}
    	
		photosList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Photo>() {

		    @Override
		    public void changed(ObservableValue<? extends Photo>observable, Photo oldValue, Photo newValue) {
		    	Photo photo=photosList.getSelectionModel().getSelectedItem();
		    	currPhoto=photo;
		    	selectedPhotoCaption.setText(photo.getCaption());
		    
		    	date.setText(photo.getDate());
		    	
		    	tagsObs.clear();
		    	for (Tag tag : photo.getTags()) {
		    		tagsObs.add(tag.getName() + ": " + tag.getValue());
		    	}
		    	
		    	tags.setItems(tagsObs);
		    	selectedAlbumPhoto.setImage(SwingFXUtils.toFXImage(photo.getImage(), null));
		    }
		});
		
		photosList.setCellFactory(param -> new ListCell<Photo>() {
	            private ImageView imageView = new ImageView();
	            
	            @Override
	            public void updateItem(Photo caption, boolean empty) {
	                super.updateItem(caption, empty);
	                if (empty) {
	                    setText(null);
	                    setGraphic(null);
	                } else {
	                	Image image = SwingFXUtils.toFXImage(caption.getImage(), null);
	                	imageView.setImage(image);
	                	imageView.setFitHeight(100);
	                	imageView.setFitWidth(100);
	                    setText(caption.getCaption());
	                    setGraphic(imageView);
	                }
	            }
	        });
		  
		// Adds menu items to drop down buttons based on current user.
		for (Album album : currUser.getAlbums().getAlbums()) {
			MenuItem copy = new MenuItem(album.getName());
			// adds listener to item in copy drop down
			copy.setOnAction(e -> {
				Photo selectedPhoto = photosList.getSelectionModel().getSelectedItem();
				Photo newPhoto;
				BufferedImage img = null;
				try {
					img = ImageIO.read(new File(selectedPhoto.getFileLocation()));
				} catch (IOException e1){
					e1.printStackTrace();
				}
				String caption = selectedPhoto.getCaption();
				
				Calendar cal = Calendar.getInstance();
				cal.set(Calendar.MILLISECOND,0);
				Date calDate = cal.getTime();
				String pattern = "MM/dd/yyyy";
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
				String date = simpleDateFormat.format(calDate);
				
				String fileLoc = selectedPhoto.getFileLocation();
				
				newPhoto = new Photo(img, caption, fileLoc, date);
				for (Tag tag : selectedPhoto.getTags()) {
					newPhoto.addTag(tag);
				}
				album.addImage(newPhoto);
				newPhoto.setAlbum(album);
				
				if (album==currAlbum) {
					photosList.getItems().add(newPhoto);
				}
				
			});
			copyTo.getItems().add(copy);

			MenuItem move = new MenuItem(album.getName());
			// Adds listener to item in move drop down
			move.setOnAction(e ->{
				Photo selectedPhoto = photosList.getSelectionModel().getSelectedItem();
				Photo newPhoto;
				BufferedImage img = null;
				try {
					img = ImageIO.read(new File(selectedPhoto.getFileLocation()));
				} catch (IOException e1){
					e1.printStackTrace();
				}
				String caption = selectedPhoto.getCaption();
				
				Calendar cal = Calendar.getInstance();
				cal.set(Calendar.MILLISECOND,0);
				Date calDate = cal.getTime();
				String pattern = "MM/dd/yyyy";
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
				String date = simpleDateFormat.format(calDate);
				
				String fileLoc = selectedPhoto.getFileLocation();
				
				newPhoto = new Photo(img, caption, fileLoc, date);
				for (Tag tag : selectedPhoto.getTags()) {
					newPhoto.addTag(tag);
				}
				currAlbum.removeImage(selectedPhoto);
				photosList.getItems().remove(selectedPhoto);
				album.addImage(newPhoto);
				newPhoto.setAlbum(album);
				if (currAlbum==album) {
					photosList.getItems().add(newPhoto);
				}
			});
			moveTo.getItems().add(move);
		}
		
		slideshow.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				// Sends to slideshow page
				if (currAlbum.getPhotoCount()==0) {
					error("You need to add photos to make a slideshow", mainStage);
					return;
				}
				FXMLLoader loader = new FXMLLoader();
		        loader.setLocation(getClass().getResource("/view/Slideshow.fxml"));
		   	    VBox root = null;
				try {
					root = (VBox)loader.load();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
		        mainStage.setScene(new Scene(root, 900, 600));
		        
		        SlideshowController slideshow_Controller = (SlideshowController) loader.getController();
		        slideshow_Controller.start(mainStage);
		        
		        mainStage.show();
			}
		});
		
		backToAlbums.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				// Sends back to albums page (homepage)
				FXMLLoader homepageLoader = new FXMLLoader();
		        homepageLoader.setLocation(getClass().getResource("/view/Homepage.fxml"));
		   	    Pane root = null;
				try {
					root = (Pane)homepageLoader.load();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
		   	    
		        mainStage.setScene(new Scene(root, 900, 600));
		        
		        HomeController home_controller = homepageLoader.getController();
		        home_controller.start(mainStage);
		        mainStage.show();
			}
		});
		
		logout.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				try {
					 // Empties file before writing to it
					 File file = new File("src/model/users.dat");
					 PrintWriter pw = new PrintWriter(file);
					 pw.write("");
					 pw.close();
					 
			         FileOutputStream fileOut =
			         new FileOutputStream("src/model/users.dat");
			         ObjectOutputStream out = new ObjectOutputStream(fileOut);
			         out.writeInt(User.getUsers().size()); 
			         for (User user : User.getUsers()) {
			        	 out.writeObject(user);
			         }
			         out.close();
			         fileOut.close();
			         System.out.println("Serialized data is saved in users.dat");
			      } catch (IOException i) {
			         i.printStackTrace();
			      }
				// Sends back to login page
				FXMLLoader loader = new FXMLLoader();
		        loader.setLocation(getClass().getResource("/view/Login.fxml"));
		   	    Pane root = null;
				try {
					root = (Pane)loader.load();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
		        mainStage.setScene(new Scene(root, 600, 400));
		        LoginController login_Controller = (LoginController) loader.getController();
		        login_Controller.start(mainStage);
		        LoginController.uloggedIn = null;
		        mainStage.show();
			}
		});
		
		addPhoto.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				Stage addPhoto = new Stage();
				addPhoto.setTitle("Choose a Photo");
				FileChooser fileChooser = new FileChooser();
				File file = fileChooser.showOpenDialog(addPhoto);
				
				if(file!=null) {
					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.MILLISECOND,0);
					Date calDate = cal.getTime();
					String pattern = "MM/dd/yyyy";
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
					String date = simpleDateFormat.format(calDate);
					
					String fileLoc = file.getPath();
					
					BufferedImage buffimg = null;
					try {
						buffimg = ImageIO.read(new File(fileLoc));
					} catch (IOException e1){
						e1.printStackTrace();
					}
					
					Photo newPhoto = new Photo(buffimg, file.getName(), fileLoc, date);
					currAlbum.addImage(newPhoto);
					newPhoto.setAlbum(currAlbum);
					photosList.getItems().add(newPhoto);
				}
			}
		});
		
		editPhoto.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				// Sends to edit photo page
				FXMLLoader loader = new FXMLLoader();
		        loader.setLocation(getClass().getResource("/view/EditPhoto.fxml"));
		   	    VBox root = null;
				try {
					root = (VBox)loader.load();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
		        mainStage.setScene(new Scene(root, 400, 600));
		        EditPhotoController edit_Controller = (EditPhotoController) loader.getController();
		        edit_Controller.start(mainStage);
		        mainStage.show();
			}
		});
		
		removePhoto.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				Photo deletedPhoto = photosList.getSelectionModel().getSelectedItem();
				photosList.getItems().remove(deletedPhoto);
			}
		});
	}
	
	/**
	 * Displays information box with error information.
	 * @param message Message describing the error.
	 * @param mainStage Stage where the error appears.
	 */
	private void error(String message, Stage mainStage) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Error");
	    alert.initOwner(mainStage);
    	alert.setHeaderText(message);
		alert.showAndWait();
		return;
	}
}

