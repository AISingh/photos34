package view;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Pair;
import model.Album;
import model.Photo;
import model.Tag;
import model.User;

public class SearchController {
	@FXML
	Button logout = new Button();
	@FXML
	TextField tagInput = new TextField();
	@FXML
	Button searchTags = new Button();
	@FXML
	Button searchDate = new Button();
	@FXML
	Button back = new Button();
	@FXML
	ListView<Photo> searchResults = new ListView<>();
	@FXML
	ImageView albumPic = new ImageView();
	@FXML
	ListView<String> tags = new ListView<>();
	@FXML
	Text albumCaption = new Text();
	@FXML
	Button createAlbum = new Button();
	@FXML
	Text albumDate = new Text();
	@FXML
	TextField newTitle = new TextField();
	@FXML
	Button editPhoto = new Button();
	@FXML
	Text albumName = new Text();
	@FXML
	Text resultsName = new Text();
	
	private String searchQuery;
	private ObservableList<Photo> resultsObs = FXCollections.observableArrayList();
	private ObservableList<String> tagsObs = FXCollections.observableArrayList();
	
	public void start(Stage mainStage) {
		User currUser = LoginController.uloggedIn;

		Album alb = currUser.getCurrAlbum();
		
		if(alb.getPhotoCount()==0) {
			error("no albums to display",mainStage);
			return;
		}

		resultsObs.addAll(alb.getPhotos());
		
		searchResults.setItems(resultsObs);
		searchResults.getSelectionModel().select(0);
		Photo selected = searchResults.getSelectionModel().getSelectedItem();
		for (Tag tag : selected.getTags()) {
			tagsObs.add(tag.getName() + ": " + tag.getValue());
		}
		tags.setItems(tagsObs);
		albumName.setText(selected.getAlbum().getName());
		resultsName.setText(albumName.getText());
		albumDate.setText(selected.getDate());
		albumCaption.setText(selected.getCaption());
		albumPic.setImage(selected.convertToFXImage());
		albumPic.setFitHeight(191);
		albumPic.setFitWidth(322);
		
		searchResults.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Photo>() {

		    @Override
		    public void changed(ObservableValue<? extends Photo>observable, Photo oldValue, Photo newValue) {
		    	Photo photo=searchResults.getSelectionModel().getSelectedItem();
		    	albumCaption.setText(photo.getCaption());
		    
		    	albumDate.setText(photo.getDate());
		    	
		    	tagsObs.clear();
		    	for (Tag tag : photo.getTags()) {
		    		tagsObs.add(tag.getName() + ": " + tag.getValue());
		    	}
		    	
		    	tags.setItems(tagsObs);
		    	albumName.setText(photo.getAlbum().getName());
				albumDate.setText(photo.getDate());
				albumCaption.setText(photo.getCaption());
				
		    	albumPic.setImage(photo.convertToFXImage());
		    	albumPic.setFitHeight(191);
				albumPic.setFitWidth(322);
		    	
		    }
		});
		
		searchResults.setCellFactory(param -> new ListCell<Photo>() {
            private ImageView imageView = new ImageView();
            
            @Override
            public void updateItem(Photo photo, boolean empty) {
                super.updateItem(photo, empty);
                if (empty) {
                    setText(null);
                    setGraphic(null);
                } else {
                	Image image = SwingFXUtils.toFXImage(photo.getImage(), null);
                	imageView.setImage(image);
                	imageView.setFitHeight(100);
                	imageView.setFitWidth(100);
                    setText(photo.getCaption());
                    setGraphic(imageView);
                }
            }
        });
		
		searchTags.setOnAction(e -> {
			searchByTags(mainStage);
		});
		
		createAlbum.setOnAction(e -> {
			if (newTitle.getText().isEmpty()) {
				error("You must enter a title for the album.", mainStage);
				return;
			}
			if(searchResults.getItems().isEmpty()) {
				error("There must be resulting photos from the search to make an album.", mainStage);
				return;
			}
			String name = newTitle.getText();
			Album album = new Album(name);
			for (int i=0; i<searchResults.getItems().size(); i++) {
				Photo photo = searchResults.getItems().get(i);
				album.addImage(photo);
			}
			currUser.getAlbums().getAlbums().add(album);
			success("Go to homepage to see new album", mainStage);
		});
		
		editPhoto.setOnAction(e -> {
			// Sends to edit photo page
			FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(getClass().getResource("/view/EditPhoto.fxml"));
	   	    VBox root = null;
			try {
				root = (VBox)loader.load();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	        mainStage.setScene(new Scene(root, 400, 600));
	        
	        EditPhotoController edit_Controller = (EditPhotoController) loader.getController();
	        edit_Controller.start(mainStage);
	        
	        mainStage.show();
		});
		
		back.setOnAction(e -> {
			// Sends back to albums page (homepage)
			FXMLLoader homepageLoader = new FXMLLoader();
	        homepageLoader.setLocation(getClass().getResource("/view/Homepage.fxml"));
	   	    Pane root = null;
			try {
				root = (Pane)homepageLoader.load();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	   	    
	        mainStage.setScene(new Scene(root, 900, 600));
	        
	        HomeController home_controller = homepageLoader.getController();
	        home_controller.start(mainStage);
	        mainStage.show();
		});
		
		searchDate.setOnAction(e -> {
			searchByDate(mainStage);
		});
		
		logout.setOnAction(e -> {
				try {
					 // Empties file before writing to it
					 File file = new File("src/model/users.dat");
					 PrintWriter pw = new PrintWriter(file);
					 pw.write("");
					 pw.close();
					 
			         FileOutputStream fileOut =
			         new FileOutputStream("src/model/users.dat");
			         ObjectOutputStream out = new ObjectOutputStream(fileOut);
			         out.writeInt(User.getUsers().size()); 
			         for (User user : User.getUsers()) {
			        	 out.writeObject(user);
			         }
			         out.close();
			         fileOut.close();
			         System.out.println("Serialized data is saved in users.dat");
			      } catch (IOException i) {
			         i.printStackTrace();
			      }
				// Sends back to login page
				FXMLLoader loader = new FXMLLoader();
		        loader.setLocation(getClass().getResource("/view/Login.fxml"));
		   	    Pane root = null;
				try {
					root = (Pane)loader.load();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
		        mainStage.setScene(new Scene(root, 600, 400));
		        LoginController login_Controller = (LoginController) loader.getController();
		        login_Controller.start(mainStage);
		        LoginController.uloggedIn = null;
		        mainStage.show();
		});
	}
	
	private void error(String message, Stage mainStage) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Error");
	    alert.initOwner(mainStage);
    	alert.setHeaderText(message);
		alert.showAndWait();
		return;
	}
	
	private void success(String message, Stage mainStage) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Success");
	    alert.initOwner(mainStage);
    	alert.setHeaderText(message);
		alert.showAndWait();
		return;
	}
	
	public void searchByDate(Stage mainStage) {
		
		Dialog<Pair<LocalDate, LocalDate>> dialog = new Dialog<>();
		dialog.setTitle("Date Search");
		dialog.setHeaderText("Pick a date range to search.");

		ButtonType buttonType = new ButtonType("Search", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(buttonType, ButtonType.CANCEL);

		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 100, 10, 10));

		DatePicker start = new DatePicker();
		DatePicker end = new DatePicker();

		grid.add(new Label("Start:"), 0, 0);
		grid.add(start, 1, 0);
		grid.add(new Label("End:"), 0, 1);
		grid.add(end, 1, 1);

		dialog.getDialogPane().setContent(grid);

		dialog.setResultConverter(dialogButton -> {
			if (dialogButton == buttonType) {
				return new Pair<>(start.getValue(), end.getValue());
			}
			return null;
		});

		Optional<Pair<LocalDate, LocalDate>> result = dialog.showAndWait();

		result.ifPresent(startEnd -> {
			if (startEnd.getKey().isAfter(startEnd.getValue())) {
				error("invald dates");
				return;
			}

			Date startDate = Date.from(startEnd.getKey().atStartOfDay(ZoneId.systemDefault()).toInstant());
			Date endDate = Date.from(startEnd.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
			User user = LoginController.uloggedIn;
			user.setCurrAlbum(user.getAlbums().getPhotosByDate(startDate, endDate));
			
	    	FXMLLoader loader = new FXMLLoader();
	    	loader.setLocation(getClass().getResource("/view/Search.fxml"));
	    	Pane root = null;
	    	try {
				root = (Pane)loader.load();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	        mainStage.setScene(new Scene(root));
	        SearchController search_Controller = (SearchController) loader.getController();
	        search_Controller.start(mainStage);
	        mainStage.show();
			
		});

	}
	
	public void searchByTags(Stage mainStage) {

		
		String result = HomeController.searchTags;

		ArrayList <Tag> tags=null;
		
		//"Example- stock:stock");


		if(result!=null) {
				tags = new ArrayList<Tag>();
				String[] tp = result.split(",");
				for (String tagpair : tp) {
					tagpair = tagpair.trim();
					String[] pair = tagpair.split(":");
					tags.add(new Tag(pair[0].trim(), pair[1].trim()));
				}
		}
		
		User user = LoginController.uloggedIn;
		user.setCurrAlbum(user.getAlbums().getPhotosByTags(tags));
		
    	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("/view/Search.fxml"));
    	Pane root = null;
    	try {
			root = (Pane)loader.load();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        mainStage.setScene(new Scene(root));
        SearchController search_Controller = (SearchController) loader.getController();
        search_Controller.start(mainStage);
        mainStage.show();
	}
	
	private void error(String message) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Error");
    	alert.setHeaderText(message);
		alert.showAndWait();
		return;
	}
}
