package view;

import java.io.IOException;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import model.User;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import view.AdminController;
import view.HomeController;

public class LoginController {
	@FXML
	private TextField username = new TextField();
	@FXML
	private TextField password = new TextField();
	@FXML
	private Button login = new Button();
	@FXML
	private Button exit = new Button();
	
	static User uloggedIn; //User that successfully logged in
	
	public void start(Stage mainStage) {
		
		login.setOnAction(e -> {
				
				String user = username.getText();
				String pass = password.getText();
				
				if(user.isEmpty() || pass.isEmpty()) {
					error("Username and/or password must be filled in.", mainStage);
					return;
				}
				
				if (user.equals("admin") && pass.equals("admin")) { //send to admin screen
					FXMLLoader adminLoader = new FXMLLoader();
			        adminLoader.setLocation(getClass().getResource("/view/Admin.fxml"));
			        Pane root = null;
					try {
						root = (Pane)adminLoader.load();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					mainStage.setTitle("Photos");
					mainStage.setScene(new Scene(root, 675, 425));
			        AdminController admin_controller = (AdminController) adminLoader.getController();
			        admin_controller.start(mainStage);
			        mainStage.show();
				}
				else { 
					doLogin(mainStage); //send to check account credentials
					if(!doLogin(mainStage)) {//account invalid
	    	        	error("Invalid account details.", mainStage);
	    				username.clear();
	    				password.clear();
	    				return;
					}
						
				}
			});
	    
		exit.setOnAction(e -> {
				Platform.exit();
			});
		
	}
	
//    public void enterLogin(KeyEvent keyEvent) {
//        if (keyEvent.getCode() == KeyCode.ENTER) {
//        	doLogin();
//        }
//	}
    
	private boolean doLogin(Stage mainStage) {
		//User.setUsers();
		for(User user: User.getUsers()) {
	        if(user.getUsername().equals(username.getText()) && user.getPassword().equals(password.getText())) { //account valid, send to homepage
	        	uloggedIn = null;
	        	uloggedIn = user;
	        	FXMLLoader homeloader = new FXMLLoader();
		        homeloader.setLocation(getClass().getResource("/view/Homepage.fxml"));
		        Pane root = null;
				try {
					root = (Pane)homeloader.load();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
		        mainStage.setScene(new Scene(root));
		        HomeController home_Controller = (HomeController) homeloader.getController();
		        home_Controller.start(mainStage);
		        mainStage.show();
		        return true;
	        }
		}
		return false;
	}
	
	private void error(String message, Stage mainStage) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Error");
	    alert.initOwner(mainStage);
    	alert.setHeaderText(message);
		alert.showAndWait();
		return;
	}
}
