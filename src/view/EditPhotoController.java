package view;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Optional;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Photo;
import model.Tag;
import model.User;

public class EditPhotoController {
	
	@FXML
	private TextField caption = new TextField();
	
	@FXML
	private TextField tagValue = new TextField();
	
	@FXML
	private Button save;
	
	@FXML
	private Button cancel;
	
	@FXML
	private Button deleteTag;
	
	@FXML
	private DatePicker datePhoto = new DatePicker();
	
	@FXML
	ImageView thumbnail;
	
	@FXML
	ListView<String> tags = new ListView<>();
	
	private Photo currPhoto;
	private String oldCaption;
	private LocalDate oldDate;
	private ArrayList oldTags;
	private ObservableList<String> tagsObs = FXCollections.observableArrayList();
	Tag fin = null;
	
	public void start(Stage mainStage) {
		User currUser = LoginController.uloggedIn;
		currPhoto = AlbumController.currPhoto;
		
		setPhotoInfo();
		
		oldCaption=currPhoto.getCaption();
		oldDate=currPhoto.getDateLocalDate();
		oldTags=currPhoto.getTags();
		
	  	cancel.setOnAction(e ->{
    		cancel(mainStage);
    	});
	  	
    	save.setOnAction(e -> {//back to album
    		save();
        	FXMLLoader loader = new FXMLLoader();
        	loader.setLocation(getClass().getResource("/view/Album.fxml"));
        	Pane root = null;
        	try {
    			root = (Pane)loader.load();
    		} catch (IOException e1) {
    			e1.printStackTrace();
    		}
            mainStage.setScene(new Scene(root));
            AlbumController album_Controller = (AlbumController) loader.getController();
            album_Controller.start(mainStage);
            mainStage.show();
		});
		
	}
	
    public void setPhotoInfo() {
    	
    	caption.setText(currPhoto.getCaption());
    	datePhoto.setValue(currPhoto.getDateLocalDate());
    	
		Image image = SwingFXUtils.toFXImage(currPhoto.getImage(), null);
		thumbnail.setImage(image);
		
		if(currPhoto.getTags().size()!=0) {
			for (Tag tag : currPhoto.getTags()) {
				tagsObs.add(tag.getName() + ": " + tag.getValue());
			}
    	
			tags.setItems(tagsObs);
		}
		
		tags.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

		    @Override
		    public void changed(ObservableValue<? extends String>observable, String oldValue, String newValue) {
		    	String tag=tags.getSelectionModel().getSelectedItem();
		    }
		});
		
		deleteTag.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				String deletedTag = tags.getSelectionModel().getSelectedItem();
				tags.getItems().remove(deletedTag);
		    	String tagType,tagValue;
		    	String[] tags2 = deletedTag.split(":");
		    	tagType = tags2[0];
		    	tagValue = tags2[1];
		    	Tag temp = new Tag(tagValue,tagType);
				if(currPhoto.getTags().size()!=0) {
					for (Tag t : currPhoto.getTags()) {
						if(t.equals(temp)) {
							currPhoto.removeTag(t);
						}
					}
				}
			}
		});
			
    }
    
    public void cancel(Stage mainStage) {
    	Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle("Cancel");
    	alert.setHeaderText("Cancel Changes");
    	alert.setContentText("Are you sure you want to cancel these changes?");
    	fin=null;

    	Optional<ButtonType> result = alert.showAndWait();
    	if (result.get() == ButtonType.OK){
        	FXMLLoader loader = new FXMLLoader();
        	loader.setLocation(getClass().getResource("/view/Album.fxml"));
        	Pane root = null;
        	try {
    			root = (Pane)loader.load();
    		} catch (IOException e1) {
    			e1.printStackTrace();
    		}
            mainStage.setScene(new Scene(root));
            AlbumController album_Controller = (AlbumController) loader.getController();
            album_Controller.start(mainStage);
            mainStage.show();
    	}
    	else {
    		
    	}
    }
    
	public void save() {
		if(caption.getText()!=oldCaption) {
			currPhoto.setCaption(caption.getText());
		}
		if(datePhoto.getValue()!=oldDate) {
			LocalDate localDate = datePhoto.getValue();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
			String date = localDate.format(formatter);
			currPhoto.setDate(date);
		}
		
		if(fin!=null) currPhoto.addTag(fin);
			
		try {
			 // Empties file before writing to it
			 File file = new File("src/model/users.dat");
			 PrintWriter pw = new PrintWriter(file);
			 pw.write("");
			 pw.close();
			 
	         FileOutputStream fileOut =
	         new FileOutputStream("src/model/users.dat");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeInt(User.getUsers().size()); 
	         for (User user : User.getUsers()) {
	        	 out.writeObject(user);
	         }
	         out.close();
	         fileOut.close();
	         System.out.println("Serialized data is saved in users.dat");
	      } catch (IOException i) {
	         i.printStackTrace();
	      }
	}
	
    public void addTag() {

    	String tag = tagValue.getText();

    	if (tag.replaceAll("\\s", "").isEmpty() || tag==null ){
    		error("Incorrect syantax. Must be in the format type:pair");
    		return;
    	}
    	
    	String[] tags = tag.split(":");
    	
    	if ((!tag.contains(":")) || tags.length<2 ){
    		error("Incorrect syantax. Must be in the format type:pair");
    		return;
    	}
    	
    	String tagType = tags[0].toLowerCase();
    	String tagValue = tags[1].toLowerCase();
    	
    	for (Tag t : currPhoto.getTags()) {
    		if(t.getValue()==tagValue||t.getName()==tagType) {
    			error("Tag already exists");
    			return;
    		}
    	}
    	
    	fin = new Tag(tagValue,tagType);
    	
    	tagsObs.add(tagType + ": " + tagValue);

		
    }
    
	private void error(String message) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Error");
    	alert.setHeaderText(message);
		alert.showAndWait();
		return;
	}
}
