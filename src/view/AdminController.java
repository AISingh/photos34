package view;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.User;

/**
 * Controller for Admin page. Admin username is "admin" and password
 * is "admin". Admins can add or delete users and the page displays
 * a list of all users.
 * 
 * @author Dylan Oelkers
 * @author Aman Singh
 *
 */

public class AdminController {
	@FXML
	private ListView<User> library = new ListView<User>();
	@FXML
	private TextField username = new TextField();
	@FXML
	private TextField password = new TextField();
	@FXML
	private Button deleteUser = new Button();
	@FXML
	private Button addUser = new Button();
	@FXML
	private Text adminTitle = new Text();
	@FXML
	private Button logout = new Button();
	
	/**
	 * Observable list representing users in the file.
	 */
	private ObservableList<User> users = FXCollections.observableArrayList();
	
	public void start(Stage mainStage) {
		
		// Deletes user from list.
		addUser.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				if(username.getText().isEmpty() || password.getText().isEmpty()) {
					error("Username and password must be entered to create user.", mainStage);
					return;
				}
				String user = username.getText();
				String pass = password.getText();
				
				for (User usr : library.getItems()) {
					if(usr.getUsername().equals(user)) {
						error("Username already taken.", mainStage);
						return;
					}
				}
				
				User newUser = new User(user, pass);
				User.getUsers().add(newUser);
				library.getItems().add(newUser);
				
				username.clear();
				password.clear();
			}
		});
		
		deleteUser.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				User user = library.getSelectionModel().getSelectedItem();
				
				if (user==null) {
					error("Must select a user in order to delete one.", mainStage);
					return;
				}
				
				User.getUsers().remove(user);
				library.getItems().remove(user);
			}
		});
		
		// Saves users, logs admin out and return to login screen.
		logout.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent e) {
				try {
					 // Empties file before writing to it
					 File file = new File("src/model/users.dat");
					 PrintWriter pw = new PrintWriter(file);
					 pw.write("");
					 pw.close();
					 
			         FileOutputStream fileOut =
			         new FileOutputStream("src/model/users.dat");
			         ObjectOutputStream out = new ObjectOutputStream(fileOut);
			         out.writeInt(User.getUsers().size()); 
			         for (User user : User.getUsers()) {
			        	 out.writeObject(user);
			         }
			         out.close();
			         fileOut.close();
			         System.out.println("Serialized data is saved in users.dat");
			      } catch (IOException i) {
			         i.printStackTrace();
			      }
				
				// Sends back to login page
				FXMLLoader loader = new FXMLLoader();
		        loader.setLocation(getClass().getResource("/view/Login.fxml"));
		   	    Pane root = null;
				try {
					root = (Pane)loader.load();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
		        mainStage.setScene(new Scene(root, 600, 400));
		        LoginController login_Controller = (LoginController) loader.getController();
		        login_Controller.start(mainStage);
		        mainStage.show();
			}
		});
		
		users.setAll(User.getUsers());
		library.setItems(users);
	}
	
	/**
	 * Displays information box with error information.
	 * @param message Message describing the error.
	 * @param mainStage Stage where the error appears.
	 */
	private void error(String message, Stage mainStage) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Error");
	    alert.initOwner(mainStage);
    	alert.setHeaderText(message);
		alert.showAndWait();
		return;
	}
}
