package view;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Optional;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Pair;
import model.Album;
import model.Tag;
import model.User;
import view.LoginController;

public class HomeController {

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private FlowPane flowPane;

    @FXML
    private Label albumName;

    @FXML
    private Label ePhoto;

    @FXML
    private Label numPhotos;

    @FXML
    private Label lPhoto;

    @FXML
    private Button newAlbum;

    @FXML
    private Button open;

    @FXML
    private Button logout;
    
    @FXML
    private Button searchByDate;
    
    @FXML
    private Button search;
    
    @FXML
    private ImageView albumImage;
    
    @FXML
    private Text userName = new Text();
    
    @FXML
    private TextField tags = new TextField();

    private User user;
    private Album selectedAlbum = null;
    private HashMap<Album, Label> thumbnail;
	private String username;
	static String searchTags;

    public void start(Stage mainStage) {
    	
    	user = LoginController.uloggedIn;
		username = user.getUsername();
		userName.setText("Welcome " + username);

    	setAlbumInfo(); 

    	thumbnail = new HashMap<Album, Label>();

    	for(Album a : user.getAlbums().getAlbums()) {
    		newAlbumHelper(a);
    	}
    	
    	logout.setOnAction(e -> {//back to login
    		save();
			FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(getClass().getResource("/view/Login.fxml"));
	   	    Pane root = null;
			try {
				root = (Pane)loader.load();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	        mainStage.setScene(new Scene(root, 675, 425));
	        LoginController login_Controller = (LoginController) loader.getController();
	        login_Controller.start(mainStage);
	        mainStage.show();
		});
    	
    	open.setOnAction(e ->{
    		try {
				openAlbum(mainStage);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
    	});
    	
    	searchByDate.setOnAction(e ->{
    		searchByDate(mainStage);
    	});
    	
    	if(tags.getText()!=null) {
        	search.setOnAction(e ->{
        		searchByTags(mainStage);
        	});
    	}
    }
    
    private void setAlbumInfo() {
    	
    	if(selectedAlbum == null) {
    		albumName.setText("");
        	ePhoto.setText("");
        	lPhoto.setText("");
        	numPhotos.setText("");
    		return;
    	}
    	
    	albumName.setText(selectedAlbum.getName());
    	numPhotos.setText("" + selectedAlbum.getPhotoCount());
    	ePhoto.setText(selectedAlbum.getEarliestPhotoDate());
    	lPhoto.setText(selectedAlbum.getLatestPhotoDate());
    	
    	if(selectedAlbum.getPhotoCount()!=0) {
    		Image image = SwingFXUtils.toFXImage(selectedAlbum.getPhotos().get(0).getImage(), null);
    		albumImage.setImage(image);
    	}	
    }
    
    @FXML
    private void newAlbum() {
    	TextInputDialog dialog = new TextInputDialog("Album Name");
    	dialog.setTitle("New Album");
    	dialog.setHeaderText("Add New Album");
    	dialog.setContentText("Enter the name of the new album:");

    	Optional<String> result = dialog.showAndWait();
    	result.ifPresent(name -> {
    		for(Album a : user.getAlbums().getAlbums()) {
    			if(a.getName().toLowerCase().equals(name.toLowerCase())) {
    				error("Album name already exists.");
    	    		return;
    			}
    		}
    		if (name.replaceAll("\\s", "").isEmpty()) {
    			error("Album name cannot be blank.");
	    		return;

    		}
    		Album newAlbum = new Album(name);
    		user.addAlbum(newAlbum);
    		newAlbumHelper(newAlbum);
    	});
    }

    private void openAlbum(Stage mainStage) throws IOException {
    	if(selectedAlbum == null) {
    		error("No album selected.");
    		return;
    	}
    	
    	user.setCurrAlbum(selectedAlbum);

    	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("/view/Album.fxml"));
    	Pane root = null;
    	try {
			root = (Pane)loader.load();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        mainStage.setScene(new Scene(root));
        AlbumController album_Controller = (AlbumController) loader.getController();
        album_Controller.start(mainStage);
        mainStage.show();

    }

    @FXML
    private void renameAlbum() {
    	if(selectedAlbum == null) {
    		error("No album selected.");
    		return;
    	}
    	TextInputDialog dialog = new TextInputDialog(selectedAlbum.getName());
    	dialog.setTitle("Rename Album");
    	dialog.setHeaderText("Rename Album");
    	dialog.setContentText("Enter new album name:");

    	Optional<String> result = dialog.showAndWait();
    	result.ifPresent(name -> {
    		for(Album a : user.getAlbums().getAlbums()) {
    			if(a.getName().toLowerCase().equals(name.toLowerCase())) {
    				error("Album name already exists.");
    	    		return;
    			}
        		if (name.replaceAll("\\s", "").isEmpty()) {
        			error("Album name cannot be blank.");
    	    		return;

        		}
    		}
    		selectedAlbum.rename(name);
    		Label lb = thumbnail.get(selectedAlbum);
    		lb.setText(name);
    		setAlbumInfo(); 
    	});
    }

    @FXML
    private void deleteAlbum() {
    	if(selectedAlbum == null) {
    		error("No album selected.");
    		return;
    	}
    	Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle("Delete Album");
    	alert.setHeaderText("Deleting Album");
    	alert.setContentText("Are you sure you want to delete this album?");

    	Optional<ButtonType> result = alert.showAndWait();
    	if (result.get() == ButtonType.OK){
    		flowPane.getChildren().remove(thumbnail.get(selectedAlbum));
    		thumbnail.remove(selectedAlbum);
    		user.removeAlbum(selectedAlbum);
    		selectedAlbum = null;
    		setAlbumInfo();
    	} 
    	else {
    	    
    	}
    }

    private void onMouseClicked(MouseEvent event) {

    	Label selectedLabel = ((Label)event.getSource());
    	String selectedAlbumName = selectedLabel.getText().split("\\\n")[0];
    	selectedAlbum = findAlbum(selectedAlbumName);
    	setAlbumInfo();
    }

    private void newAlbumHelper(Album album) {
    	Label newLabel = initalizeNewAlbum(album);
    	thumbnail.put(album, newLabel);
		flowPane.getChildren().add(newLabel);
    }
    
    private Album findAlbum(String albumName) {
    	for(Album a : user.getAlbums().getAlbums()) {
    		if(a.getName().equals(albumName)) {
    			return a;
    		}
    	}
    	return null;
    }

    private Label initalizeNewAlbum(Album album) {
    	Label lb = new Label();
		lb.setText(album.getName());
		lb.setOnMouseClicked(this::onMouseClicked);
		lb.getStyleClass().add("thumbnail");
			
		return lb;
    }
    
	public void searchByDate(Stage mainStage) {

		Dialog<Pair<LocalDate, LocalDate>> dialog = new Dialog<>();
		dialog.setTitle("Date Search");
		dialog.setHeaderText("Pick a date range to search.");

		ButtonType buttonType = new ButtonType("Search", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(buttonType, ButtonType.CANCEL);

		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 100, 10, 10));

		DatePicker start = new DatePicker();
		DatePicker end = new DatePicker();

		grid.add(new Label("Start:"), 0, 0);
		grid.add(start, 1, 0);
		grid.add(new Label("End:"), 0, 1);
		grid.add(end, 1, 1);

		dialog.getDialogPane().setContent(grid);

		dialog.setResultConverter(dialogButton -> {
			if (dialogButton == buttonType) {
				return new Pair<>(start.getValue(), end.getValue());
			}
			return null;
		});

		Optional<Pair<LocalDate, LocalDate>> result = dialog.showAndWait();

		result.ifPresent(startEnd -> {
			if (startEnd.getKey().isAfter(startEnd.getValue())) {
				error("invald dates");
				return;
			}

			Date startDate = Date.from(startEnd.getKey().atStartOfDay(ZoneId.systemDefault()).toInstant());
			Date endDate = Date.from(startEnd.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());

			user.setCurrAlbum(user.getAlbums().getPhotosByDate(startDate, endDate));
			
	    	FXMLLoader loader = new FXMLLoader();
	    	loader.setLocation(getClass().getResource("/view/Search.fxml"));
	    	Pane root = null;
	    	try {
				root = (Pane)loader.load();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	        mainStage.setScene(new Scene(root));
	        SearchController search_Controller = (SearchController) loader.getController();
	        search_Controller.start(mainStage);
	        mainStage.show();
			
		});

	}
	
	public void searchByTags(Stage mainStage) {

	
		String result = tags.getText();
		searchTags = result;

		ArrayList <Tag> tags=null;
		
		//"Example- stock:stock");


		if(result!=null) {
				tags = new ArrayList<Tag>();
				String[] tp = result.split(",");
				for (String tagpair : tp) {
					tagpair = tagpair.trim();
					String[] pair = tagpair.split(":");
					tags.add(new Tag(pair[0].trim(), pair[1].trim()));
				}
		}
		
		user.setCurrAlbum(user.getAlbums().getPhotosByTags(tags));
		
    	FXMLLoader loader = new FXMLLoader();
    	loader.setLocation(getClass().getResource("/view/Search.fxml"));
    	Pane root = null;
    	try {
			root = (Pane)loader.load();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
        mainStage.setScene(new Scene(root));
        SearchController search_Controller = (SearchController) loader.getController();
        search_Controller.start(mainStage);
        mainStage.show();
	}
    
	private void error(String message) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Error");
    	alert.setHeaderText(message);
		alert.showAndWait();
		return;
	}
	
	public void save() {
		try {
			 // Empties file before writing to it
			 File file = new File("src/model/users.dat");
			 PrintWriter pw = new PrintWriter(file);
			 pw.write("");
			 pw.close();
			 
	         FileOutputStream fileOut =
	         new FileOutputStream("src/model/users.dat");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeInt(User.getUsers().size()); 
	         for (User user : User.getUsers()) {
	        	 out.writeObject(user);
	         }
	         out.close();
	         fileOut.close();
	         System.out.println("Serialized data is saved in users.dat");
	      } catch (IOException i) {
	         i.printStackTrace();
	      }
	}
}